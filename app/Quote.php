<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'img', 'text', 'color', 'size', 'sound', 'type', 'author', 'lang'
    ];
    public $timestamps = false;

    public function scopeByImg($query, $img){
        return $query->where('img', 'LIKE','%'.$img.'%');
    }

    public function scopeByFilter($query, \Illuminate\Http\Request $request){
        if($request->has('img')) $query->byImg($request->get('img'));
        return $query;
    }
}
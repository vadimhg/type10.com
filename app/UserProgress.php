<?php
namespace app;
use Illuminate\Database\Eloquent\Model;

class UserProgress extends Model
{
    public $table = 'users_progress';

    public function checkAvaliable($courseId, $lessonStep = 0, $pageStep = 0){
        return $this->course_id==$courseId && $lessonStep<=$this->lesson_step && $pageStep<=$this->page_step;
    }
}
<?php

namespace App\Http\Middleware;

use Closure;

class BreadCrumbs
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $breadcrumbs = [
            ['name'=>'Home', 'url'=>route('student_index')]
        ];
        view()->share('breadcrumbs', $breadcrumbs);
        return $next($request);
    }
}

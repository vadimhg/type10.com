<?php

namespace App\Http\Middleware;

use Closure;
use App\Quote;

class GetQuote
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        view()->share('quote', Quote::orderBy(\DB::raw('RAND()'))->first());
        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
class PathData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = $request->route();
        //hasParameter(); parameter()
        try {
            if ($route->hasParameter('exam')) {
                $exam = \App\Exam::withUpData($route)->first();
                $request->attributes->add(['exam' => $exam, 'page'=>$exam->page, 'lesson'=>$exam->page->lesson, 'course'=>$exam->page->lesson->course]);
            } elseif ($route->hasParameter('page')) {
                $page = \App\Page::withUpData($route)->first();
                $request->attributes->add(['exams' => $page->exams, 'page'=>$page, 'lesson'=>$page->lesson, 'course'=>$page->lesson->course]);
            } elseif ($route->hasParameter('lesson')) {
                $lesson = \App\Page::withUpData($route)->first();
                $request->attributes->add(['pages'=>$lesson->pages, 'lesson'=>$lesson, 'course'=>$lesson->course]);
            } elseif ($route->hasParameter('course')) {
                $course = \App\Course::withUpData($route)->first();
                $request->attributes->add(['lesson'=>$course->lessons, 'course'=>$course]);
            }
        } catch(RouteNotFoundException $ex){
            
        }
        //$request->attributes->add(['key' => '1'] );
        return $next($request);
    }
}

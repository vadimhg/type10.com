<?php
namespace app\Http\Controllers\web\student;

use App\Http\Controllers\Controller;
use App\Course;
class IndexController extends Controller
{
    public function index(){
        return view('web.student.index', ['courses'=>Course::all()]);
    }
}
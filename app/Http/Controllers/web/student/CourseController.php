<?php
namespace app\Http\Controllers\web\student;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;

class CourseController extends Controller
{
    public function index(){
        return view('web.student.course.index');
    }

    public function lessons(Guard $auth, $courseId){
        //$currentProgress = $auth->user()->progress
        return view('web.student.course.lessons');
    }

    public function pages(Guard $auth, $courseId, $lessonStep){
        return view('web.student.course.pages');
    }

    public function page(Guard $auth, $courseId, $lessonStep, $pageStep){
        return view('web.student.course.page');
    }
}
<?php
namespace app\Http\Controllers\web\admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExportController extends Controller
{
    public function groups(Request $request)
    {
        return response(\App\CourseGroup::byFilter($request)->get()->toJson(), 200, [
            'Content-Type'=>'application/octet-stream',
            'Content-Disposition'=>'attachment; filename="groups.json"'
        ]);
    }

    public function courses(Request $request)
    {
        return response(\App\Course::byFilter($request)->get()->toJson(), 200, [
            'Content-Type'=>'application/octet-stream',
            'Content-Disposition'=>'attachment; filename="courses.json"'
        ]);
    }

    public function lessons(Request $request)
    {
        return response(\App\Lesson::byFilter($request)->get()->toJson(), 200, [
            'Content-Type'=>'application/octet-stream',
            'Content-Disposition'=>'attachment; filename="lessons.json"'
        ]);
    }

    public function pages(Request $request)
    {
        return response(\App\Page::byFilter($request)->get()->toJson(), 200, [
            'Content-Type'=>'application/octet-stream',
            'Content-Disposition'=>'attachment; filename="pages.json"'
        ]);
    }

    public function quotes()
    {
        return response(\App\Quote::all()->toJson(), 200, [
            'Content-Type'=>'application/octet-stream',
            'Content-Disposition'=>'attachment; filename="quotes.json"'
        ]);
    }

    public function texts()
    {
        return response(\App\Text::all()->toJson(), 200, [
            'Content-Type'=>'application/octet-stream',
            'Content-Disposition'=>'attachment; filename="texts.json"'
        ]);
    }

    public function exams()
    {
        return response(\App\Exam::all()->toJson(), 200, [
            'Content-Type'=>'application/octet-stream',
            'Content-Disposition'=>'attachment; filename="exams.json"'
        ]);
    }
}

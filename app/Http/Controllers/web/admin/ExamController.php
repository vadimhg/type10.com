<?php
namespace app\Http\Controllers\web\admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exam;
use Validator;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('web.admin.exams.index', ['exams'=>Exam::with('page')->byFilter($request)->paginate(15)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('web.admin.exams.create',['texts'=>\App\Text::where('exam', 'no')->get()->pluck('title', 'id')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'title'       => 'required',
            'text_id' => 'required|numeric'
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput($request->all());
        } else {
            $exam = new Exam();
            $exam->changeText($request->input('text_id'));
            $exam->fill($request->all());
            $exam->save();
            $request->session()->flash('success', 'Successfully created!');
            return redirect()->route('exam.index', ['page_id'=>$request->get('page_id')]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Exam $exam)
    {
        return view('web.admin.exams.edit', ['exam'=>$exam, 'texts'=>\App\Text::where('exam', 'no')->orWhere('id', $exam->text_id)->get()->pluck('title', 'id')]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Exam $exam)
    {
        $rules = array(
            'title'       => 'required',
            'text_id' => 'required|numeric'
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            $exam->fill($request->all());
            $exam->changeText($request->input('text_id'));
            $exam->save();
            $request->session()->flash('success', 'Successfully updated !');
            return redirect()->route('exam.index', ['page_id'=>$exam->page_id]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Exam $exam)
    {
        $exam->delete();
        $request->session()->flash('success', 'Successfully deleted!');
        return back();
    }
}

<?php
namespace app\Http\Controllers\web\admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CourseGroup;

class CourseGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('web.admin.groups.index', ['courseGroups'=>CourseGroup::byFilter($request)->paginate(15)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //@todo: Languages - get
        return view('web.admin.groups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //@todo
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(CourseGroup $courseGroup)
    {
        //@todo: Languages - get
        return view('web.admin.groups.edit', ['courseGroup'=>$courseGroup]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//@todo
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, CourseGroup $group)
    {
        $group->delete();
        $request->session()->flash('success', 'Successfully deleted!');
        return back();
    }
}

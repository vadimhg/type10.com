<?php
namespace app\Http\Controllers\web\admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Quote;
use Validator;

class QuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('web.admin.quotes.index', ['quotes'=>Quote::byFilter($request)->paginate(15)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('management.business.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {//@todo

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Quote $quote)
    {
        return view('web.admin.quotes.edit', ['quote'=>$quote]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quote $quote)
    {
        $rules = array(
            'img'       => 'required',
            'color'       => 'required',
            'size'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            $quote->fill($request->all());
            $quote->save();
            $request->session()->flash('success', 'Successfully updated !');
            return redirect()->route('quote.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Quote $quote)
    {
        $quote->delete();
        $request->session()->flash('success', 'Successfully deleted!');
        return back();
    }
}

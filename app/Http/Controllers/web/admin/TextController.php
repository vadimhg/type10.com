<?php
namespace app\Http\Controllers\web\admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Text;

class TextController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('web.admin.texts.index', ['texts'=>Text::with('exam')->byFilter($request)->paginate(15)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {//@todo
        return view('web.admin.texts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {//@todo
        try {
            $business = new Business();
            $business->fill($request->all());
            $business->insta_handle = str_replace('@','',$request->get("insta_handle"));
            $business->formWorkinHoursText($request);
            $business->save();
            return redirect("/management/business");
        } catch(\Exception $ex){
            //todo: flash message here
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {//@todo
        return view('management.business.edit', ['business'=>Business::with('interests')->find($id), 'interests'=>\App\Interests::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {//@todo
        try {
            $business = Business::find($id);
            $business->fill($request->all());
            $business->insta_handle = str_replace('@','',$request->get("insta_handle"));
            $business->formWorkinHoursText($request);
            $business->save();
            return redirect("/management/business");
        } catch(\Exception $ex){
            //todo: flash message here
        }
        return redirect("/management/business/$id/edit");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Text $text)
    {
        $text->delete();
        $request->session()->flash('success', 'Successfully deleted!');
        return back();
    }
}

<?php
namespace app\Http\Controllers\web\admin;
use App\Http\Controllers\Controller;
use App\CourseGroup;

class IndexController extends Controller
{
    public function index(){
        return view('web.admin.index');
    }
}
<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Lesson extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'content', 'step'
    ];

    public function course(){
        return $this->belongsTo('App\Course');
    }

    public function scopeByCourseId($query, $courseId){
        return $query->where('course_id', $courseId);
    }

    public function scopeByFilter($query, \Illuminate\Http\Request &$request){
        if($request->has('course_id')) $query->byCourseId($request->get('course_id'));
        return $query;
    }
}


<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'content', 'step'];

    public function lesson(){
        return $this->belongsTo('App\Lesson');
    }

    public function scopeByLessonId($query, $lessonId){
        return $query->where('lesson_id', $lessonId);
    }

    public function scopeByFilter($query, \Illuminate\Http\Request &$request){
        if($request->has('lesson_id')) $query->byLessonId($request->get('lesson_id'));
        return $query;
    }
}
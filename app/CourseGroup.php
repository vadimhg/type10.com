<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class CourseGroup extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'step', 'active', 'lang',
    ];

    public function scopeByFilter($query, \Illuminate\Http\Request &$request){
        return $query;
    }
}
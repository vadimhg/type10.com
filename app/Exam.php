<?php
/**
 * Created by PhpStorm.
 * User: Вадим
 * Date: 20.11.2017
 * Time: 20:49
 */

namespace App;
use Illuminate\Database\Eloquent\Model;
class Exam extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'step', 'page_id', 'text_id'];

    public function page(){
        return $this->belongsTo('App\Page');
    }

    public function text(){
        return $this->belongsTo('App\Text');
    }

    public function scopeByPageId($query, $pageId){
        return $query->where('page_id', $pageId);
    }

    public function scopeByFilter($query, \Illuminate\Http\Request $request){
        if($request->has('page_id')) $query->byPageId($request->get('page_id'));
        return $query;
    }

    public function changeText($textId){
        if($textId!=$this->text_id){
            if(isset($this->text_id)) {
                $this->text->exam = 'no';
                $this->text->save();
            }
            $text = Text::find($textId);
            $text->exam = 'yes';
            $text->save();
        }
    }
}
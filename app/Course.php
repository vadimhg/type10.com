<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Course extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'step', 'active', 'description', 'available', 'subscribers'
    ];

    public function group(){
        return $this->belongsTo('App\CourseGroup', 'course_group_id');
    }

    public function scopeByGroupId($query, $groupId){
        return $query->where('course_group_id', $groupId);
    }

    public function scopeByFilter($query, \Illuminate\Http\Request &$request){
        if($request->has('group_id')) $query->byGroupId($request->get('group_id'));
        return $query;
    }
}
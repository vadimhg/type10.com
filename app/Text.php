<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Text extends Model
{
    protected $fillable = ['title', 'exam', 'complexity', 'content'];

    public function exam(){
        return $this->belongsTo('App\Exam');
    }

    public function scopeByFilter($query, \Illuminate\Http\Request $request){
        return $query;
    }
}
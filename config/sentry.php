<?php

return array(
    'dsn' => env('SENTRY_DSN', 'https://16da4f6690fb45d0ba2e3f4318f5d140@sentry.io/259580'),

    // capture release as git sha
    // 'release' => trim(exec('git log --pretty="%h" -n1 HEAD')),

    // Capture bindings on SQL queries
    'breadcrumbs.sql_bindings' => true,

    // Capture default user context
    'user_context' => true,
);

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLessonsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::defaultStringLength(255);
		Schema::create('lessons', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 255);
			$table->text('description', 65535);
			$table->text('content', 65535);
			$table->integer('step')->default(0);
			$table->integer('course_id')->unsigned()->index('lessons_course_id_foreign');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lessons');
	}

}

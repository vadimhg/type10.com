<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoursesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::defaultStringLength(255);
		Schema::create('courses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->text('description', 65535);
			$table->integer('step')->default(0);
			$table->enum('active', array('yes','no'))->default('yes');
			$table->enum('available', array('yes','after','no'))->default('yes');
			$table->enum('subscribers', array('yes','no'))->default('no');
			$table->integer('course_group_id')->unsigned()->index('courses_course_group_id_foreign');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('courses');
	}

}

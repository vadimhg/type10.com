<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateQuotesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('quotes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('img')->default('blank.png');
			$table->text('text', 65535);
			$table->integer('size')->default(20);
			$table->string('sound', 256)->nullable();
			$table->string('color', 18)->default('#000000');
			$table->enum('type', array('regular','sparse','secret'))->default('regular');
			$table->enum('lang', array('rus','eng'))->default('rus');
			$table->integer('quotes_author_id')->unsigned()->nullable()->index('quotes_quotes_author_id_foreign');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('quotes');
	}

}

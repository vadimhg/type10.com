<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserProgressTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_progress', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('course_id')->unsigned()->index('users_progress_course_id_foreign');
			$table->integer('lesson_step')->unsigned();
			$table->integer('page_step')->unsigned();
			$table->text('exams', 65535);
			$table->timestamps();
			$table->enum('certificate', array('yes','no'))->default('no');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_progress');
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExamsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::defaultStringLength(255);
		Schema::create('exams', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 255);
			$table->integer('step')->default(0);
			$table->integer('page_id')->unsigned()->index('exams_page_id_foreign');
			$table->integer('text_id')->unsigned()->index('exams_text_id_foreign');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('exams');
	}

}

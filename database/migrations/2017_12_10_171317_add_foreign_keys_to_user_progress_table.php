<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUserProgressTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_progress', function(Blueprint $table)
		{
			$table->foreign('course_id', 'users_progress_course_id_foreign')->references('id')->on('courses')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_progress', function(Blueprint $table)
		{
			$table->dropForeign('users_progress_course_id_foreign');
		});
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToQuotesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('quotes', function(Blueprint $table)
		{
			$table->foreign('quotes_author_id')->references('id')->on('quotes_authors')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('quotes', function(Blueprint $table)
		{
			$table->dropForeign('quotes_quotes_author_id_foreign');
		});
	}

}

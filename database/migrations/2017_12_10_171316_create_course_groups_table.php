<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCourseGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::defaultStringLength(255);
		Schema::create('course_groups', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->integer('step')->default(0);
			$table->enum('active', array('yes','no'))->default('yes');
			$table->enum('lang', array('rus','eng'))->default('rus');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('course_groups');
	}

}

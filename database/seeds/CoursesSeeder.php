<?php

use Illuminate\Database\Seeder;

class CoursesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('exams')->delete();
        DB::table('pages')->delete();
        DB::table('texts')->delete();
        DB::table('lessons')->delete();
        DB::table('courses')->delete();
        DB::table('course_groups')->delete();
        $json = File::get('database/data/texts.json');
        $data = json_decode($json);
        foreach($data as $key=>$obj){
            $text = new \App\Text();
            $text->id = $obj->id;
            $textLength = strlen($obj->content);
            if($textLength<30) {
                $complexity = 'very_easy';
            } elseif($textLength<100){
                $complexity = 'easy';
            } elseif($textLength<500){
                $complexity = 'normal';
            } elseif($textLength<1000){
                $complexity = 'hard';
            } elseif($textLength<10000){
                $complexity = 'very_hard';
            } else {
                $complexity = 'insane';
            }
            $text->fill([
                "title"=> $obj->title,
                "exam"=> $obj->exam,
                "complexity"=> $complexity,
                "content"=> $obj->content
            ]);
            $text->save();
        }

        $json = File::get('database/data/groups.json');
        $data = json_decode($json);
        foreach($data as $key=>$obj){
            $courseGroup = new \App\CourseGroup();
            $courseGroup->id = $obj->id;
            $courseGroup->fill([
                "title"=> $obj->title,
                "step"=> $obj->step,
                "active"=> $obj->active,
                "lang"=> $obj->lang,
            ]);
            $courseGroup->save();
        }

        $json = File::get('database/data/courses.json');
        $data = json_decode($json);
        foreach($data as $obj){
            $course = new \App\Course();
            $course->id = $obj->id;
            $course->fill([
                'title'=>$obj->title,
                'step'=>$obj->step,
                'active'=>$obj->active,
                'description'=>$obj->description,
                'available'=>$obj->available,
                'subscribers'=>$obj->subscribers
            ]);
            $course->course_group_id = $obj->course_group_id;
            $course->save();
        }

        $json = File::get('database/data/lessons.json');
        $data = json_decode($json);
        foreach($data as $obj){
            $lessons = new \App\Lesson();
            $lessons->id = $obj->id;
            $lessons->fill([
                'title'=>$obj->title,
                'step'=>$obj->step,
                'description'=>$obj->description,
                'content'=>$obj->content
            ]);
            $lessons->course_id = $obj->course_id;
            $lessons->save();
        }

        $json = File::get('database/data/pages.json');
        $data = json_decode($json);
        foreach($data as $obj){
            $pages = new \App\Page();
            $pages->id = $obj->id;
            $pages->fill([
                'title'=>$obj->title,
                'step'=>$obj->step,
                'content'=>$obj->content
            ]);
            $pages->lesson_id = $obj->lesson_id;
            $pages->save();
        }

        $json = File::get('database/data/exams.json');
        $data = json_decode($json);
        $index = 0;
        foreach($data as $obj){
            $exam = new \App\Exam();
            $exam->id = ++$index;
            $exam->fill([
                'title'=>$obj->title,
                'step'=>$obj->step,
            ]);
            $exam->page_id = $obj->page_id;
            $exam->text_id = $obj->text_id;
            $exam->save();
            $text = \App\Text::find($obj->text_id);
            $text->exam = 'yes';
            $text->save();
        }
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class QuotesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('quotes')->truncate();
        $json = File::get('database/data/quotes.json');
        $data = json_decode($json);
        foreach($data as $obj){
            \App\Quote::create([
                "id"=> $obj->id,
                "img"=> $obj->img,
                "text"=> $obj->text,
                "color"=> $obj->color,
                "size"=> $obj->size,
                "sound"=> $obj->sound,
                "type"=> $obj->type,
                "lang"=> $obj->lang,
                "quotes_author_id"=>$obj->quotes_author_id
            ]);
        }
    }
}

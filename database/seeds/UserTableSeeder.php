<?php

use Illuminate\Database\Seeder;
use App\Role;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        $role_admin = Role::where('name', 'admin')->first();
        $role_student = Role::where('name', 'student')->first();
        $role_manager  = Role::where('name', 'manager')->first();

        $admin = new App\User();
        $admin->id = 1;
        $admin->name = 'Vadim';
        $admin->email = 'vadim-job-hg@yandex.ru';
        $admin->password = bcrypt('FNX6');
        $admin->save();
        $admin->roles()->attach($role_admin);

        factory(App\User::class, 3)->create()->each(function ($u) use ($role_manager) {
            $u->roles()->attach($role_manager);
        });

        factory(App\User::class, 1000)->create()->each(function ($u) use ($role_student) {
            $u->roles()->attach($role_student);
        });
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Role;
class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();
        $role_admin = new Role();
        $role_admin->id = 1;
        $role_admin->name = 'admin';
        $role_admin->description = 'A Admin User';
        $role_admin->save();

        $role_manager = new Role();
        $role_manager->id = 2;
        $role_manager->name = 'manager';
        $role_manager->description = 'A Manager User';
        $role_manager->save();

        $role_student = new Role();
        $role_student->id = 3;
        $role_student->name = 'student';
        $role_student->description = 'A Student User';
        $role_student->save();
    }
}

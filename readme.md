<code>cp .env.example .env</code>

<code>composer update</code>

<code>unrar x -r  public/content/quotes.rar public/content</code>

<code>chmod -R 777 storage</code>

<code>sudo chmod -R 777 bootstrap/cache</code>

<code>php artisan migrate</code>

<code>php artisan db:seed --class DatabaseSeeder</code>

php artisan config:cache
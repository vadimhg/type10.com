<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Server Error</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 36px;
            padding: 20px;
        }

        .error-middle{
            width:600px;
            text-align:center;
            margin:0 auto;
        }
        .error-middle p{
            text-align: left;
            font: 12pt/10pt sans-serif;
            color: black;
            padding: 20px 0 0 0;
            line-height: 1.5;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="error-middle"><img src="/img/errors/500.png">
            <p>Ошибки сервере. Сейчас мы избиваем разработчиков. Как только мы закончим - они все починят.</p>
        </div>
    </div>
</div>
</body>
</html>

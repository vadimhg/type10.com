<!--#1 Достижения-->
@if (session('achievements'))
    @foreach(session('achievements') as $achievement)
    <div class="c-achievement">
        {{ $achievement->image }}
        {{ $achievement->part }} / {{ $achievement->full }}
        {{ $achievement->finished}}
        {{ $achievement->message }}
    </div>
    @endforeach
@endif
<!--#2 Notifications-->
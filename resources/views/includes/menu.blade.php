<nav class="navbar navbar-expand-lg bg-light justify-content-xl-between">
    <!-- Branding Image -->
    <a class="navbar-brand text-muted" href="@yield('base-link', '/')">
        TYPE <img src="/img/logo.png"  height="30" class="d-inline-block align-top" alt="" /> x 2
    </a>
    @include('includes.quote')
    <ul class="nav nav-pills">
        @guest
        <li class="nav-item"><a href="{{ route('login') }}" class="nav-link">Login</a></li>
        <li class="nav-item"><a href="{{ route('register') }}" class="nav-link">Register</a></li>
        @else
        @yield('top-menu', '')
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>
            <div class="dropdown-menu" style="left: auto; right:0;">
                @yield('dropdown-menu', '')
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    Logout
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </li>
        @endguest
    </ul>
</nav>
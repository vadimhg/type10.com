@if(isset($quote))
<div class="quotes-bar nav-item" style="background: url('/content/quotes/{{$quote->img}}') no-repeat #FFFFE0">
<div class="align-baseline" style="color:#{{ $quote->color }}; font-size:{{ $quote->size }}px; height: 35px;">
{!! $quote->text !!}
</div>
</div>
@endif
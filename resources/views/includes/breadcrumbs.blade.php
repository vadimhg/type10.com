@if(isset($breadcrumbs))
<nav aria-label="breadcrumb" role="navigation">
    <ol class="breadcrumb">
        @foreach($breadcrumbs as $key=>$breadcrumb)
        @if(!next($breadcrumbs))
        <li class="breadcrumb-item active" aria-current="page">{{$breadcrumb['name']}}</li>
        @else
        <li class="breadcrumb-item "><a href="{{$breadcrumb['url']}}">{{$breadcrumb['name']}}</a></li>
        @endif
        @endforeach
    </ol>
</nav>
@endif
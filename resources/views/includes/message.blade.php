<div class="modal fade" tabindex="-1" role="dialog" id="info_popup">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header alert">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body"></div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
    function infoPopup(title, text, type){
        var types = {
            'info':'alert-info',
            'success':'alert-success',
            'warning':'alert-warning',
            'danger':'alert-danger'
        };
        var info_popup = $("#info_popup");
        info_popup
                .find('.modal-header')
                .removeClass('alert-info alert-success alert-warning alert-danger')
                .addClass(types[type])
                .find('.modal-title')
                .text(title);
        info_popup.find('.modal-body').text(text);
        info_popup.modal('show');
    }
</script>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Type 10 - @yield('title', 'welcome')</title>
    @section('styles')
    <link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous"-->
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    @show
</head>
<body>
    <header>
    @include('includes.menu')
    </header>
    <div class="container bg-white page-content">
    @include('includes.flash')
    @yield('breadcrumbs')
    @yield('content')
    </div>
    <footer class="bg-dark text-white mt-4 footer">
        <div class="container-fluid py-3">
            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-3 text-right small align-self-end">
                    <div class="btn-group" data-toggle="buttons">
                        <a class="btn btn-secondary" target="_blank" href="#">FB</a>
                        <a class="btn btn-secondary" target="_blank" href="#">G+</a>
                        <a class="btn btn-secondary" target="_blank" href="#">T</a>
                        <a class="btn btn-secondary" href="">About Us</a>
                        <a class="btn btn-secondary" href="">Contact Us</a>
                    </div>
                </div>
                <div class="col-md-3 text-right small align-self-end">©2018 Type10, Inc.</div>
            </div>
        </div>
    </footer>
    @include('includes.message')
    @section('scripts')
    <!-- Scripts -->
    <!--script src="{{ asset('js/app.js') }}"></script-->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    @show
</body>
</html>

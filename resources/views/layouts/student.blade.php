@extends('layouts.app')
@section('base-link', route('student_index'))
@section('top-menu')
<li class="nav-item"><a href="" class="nav-link">Courses</a></li>
@endsection
@section('dropdown-menu')
    <a class="dropdown-item" href="#">
        Profile Edit
    </a>
    <a class="dropdown-item" href="#">
        Achievements
    </a>
    <div class="dropdown-divider"></div>
@endsection
@section('breadcrumbs')
@include('includes.breadcrumbs')
@endsection
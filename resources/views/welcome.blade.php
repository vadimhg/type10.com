@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="starter-template">
            <table style="width:100%">
            <tr>
                <td class="text-center">
                    <img src="/img/gits.gif">
                </td>
                <td class="text-center"><h2>Want to type fast?</h2></td>
            </tr>
            <tr>
                <td class="text-center"><h2>Want to type blind?</h2></td>
                <td class="text-center">
                    <img src="/img/bruse.gif">
                </td>
            </tr>
            <tr>
                <td class="text-center">
                    <img src="/img/200.gif">
                </td>
                <td class="text-center">
                    <h2>Want to type with 2 hands?</h2>
                </td>
            </tr>
                <tr>
                    <td colspan="2" class="text-center">
                        <br/><br/><br/><br/>
                        <p>Just click this <a class="btn btn-primary" href="{{ route('register') }}">Register</a></p>
                    </td>
                </tr>
            </table>
        </div>

    </div><!-- /.container -->
@endsection
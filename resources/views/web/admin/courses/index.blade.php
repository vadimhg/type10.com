@extends('layouts.admin')
@section('content')
<table class="table table-striped table-hover table-bordered" id="editabledatatable">
    <thead>
    <tr role="row">
        <th>
            Edit
        </th>
        <th>
            Title
        </th>
        <th>
            Description
        </th>
        <th>
            Step
        </th>
        <th>
            Active
        </th>
        <th>
            Available
        </th>
        <th>
            Group
        </th>
        <th>
           &nbsp;
        </th>
    </tr>
    </thead>
    <?php

    ?>
    <tbody>
    @foreach ($courses as $course)
        <tr>
            <td><a class="icon-edit" href='{{route('course.edit', ['id'=>$course->id])}}'>&nbsp;</a></td>
            <td>{{$course->title}}</td>
            <td>{{$course->desccription}}</td>
            <td>{{$course->step}}</td>
            <td>{{$course->active}}</td>
            <td>{{$course->available}}</td>
            <td>{{$course->group->title}}</td>
            <td><a class="icon-list-ul" href='{{route('lesson.index')}}?course_id={{$course->id}}'>&nbsp;</a></td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ $courses->appends(request()->input())->links('pagination.default') }}
@endsection
@extends('layouts.admin')
@section('content')
    <h1>Edit "{{ $exam->title }}"</h1>

    <!-- if there are creation errors, they will show here -->
    {{ Html::ul($errors->all(), ['class'=>'alert alert-danger']) }}

    {{ Form::model($exam, array('route' => array('exam.update', $exam->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('title', 'Title') }}
        {{ Form::text('title', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('text_id', 'Text') }}
        {{ Form::select('text_id', $texts, $exam->text_id, array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Save Exam', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}
@endsection
@section('scripts')
    @parent
    <script type="text/javascript">
        //todo: javascript validation here
    </script>
@endsection
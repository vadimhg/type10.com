@extends('layouts.admin')
@section('content')
    <a href="{{route('exam.create')}}?page_id={{request()->get('page_id')}}" type="button" class="btn btn-primary">Create New</a>
    <table class="table table-striped table-hover table-bordered" id="editabledatatable">
        <thead>
        <tr role="row">
            <th>
                Edit
            </th>
            <th>
                Title
            </th>
            <th>
                Step
            </th>
            <th>
                Page
            </th>
            <th>
                &nbsp;
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach ($exams as $exam)
            <tr>
                <td><a class="icon-edit" href='{{route('exam.edit', ['id'=>$exam->id])}}'>&nbsp;</a></td>
                <td>{{$exam->title}}</td>
                <td>{{$exam->step}}</td>
                <td>{{$exam->page->title}}</td>
                <td><a class="icon-remove s-remove-link j-remove-link" href='{{route('exam.destroy', ['exam'=>$exam->id])}}'>&nbsp;</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $exams->appends(request()->input())->links('pagination.default') }}
@endsection
@section('scripts')
    @parent
    <script type="text/javascript">
        $(document).on('click', '.j-remove-link', function(){
            event.preventDefault();
            if(confirm('A you sure want to remove exam from page?')){
                $('<form>',{
                    'action': $(this).attr('href'),
                    'method': 'POST'
                })
                .append($('<input>', {
                    'name': '_method',
                    'value': 'DELETE',
                    'type': 'hidden'
                }))
                .append('{{ csrf_field() }}')
                .appendTo('body')
                .submit();
            }
        });
    </script>
@endsection
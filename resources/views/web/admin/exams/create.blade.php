@extends('layouts.admin')
@section('content')
<h1>Create a Nerd</h1>

<!-- if there are creation errors, they will show here -->
{{ Html::ul($errors->all(), ['class'=>'alert alert-danger']) }}

{{ Form::open(array('route' => 'exam.store')) }}
{{Form::hidden('page_id', request()->get('page_id'))}}
<div class="form-group">
    {{ Form::label('title', 'Title') }}
    {{ Form::text('title', old('title'), array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('text_id', 'Text') }}
    {{ Form::select('text_id', $texts, null, array('class' => 'form-control')) }}
</div>

{{ Form::submit('Save Exam', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}
@endsection
@section('scripts')
    @parent
    <script type="text/javascript">
        //todo: javascript validation here
    </script>
@endsection
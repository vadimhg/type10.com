@extends('layouts.admin')
@section('content')
<a class="icon-create" href='{{route('course_group.create')}}'>&nbsp;Create</a>
<table class="table table-striped table-hover table-bordered" id="editabledatatable">
    <thead>
    <tr role="row">
        <th>
            Edit
        </th>
        <th>
            Title
        </th>
        <th>
            Step
        </th>
        <th>
            Active
        </th>
        <th>
            Lang
        </th>
        <th>
           &nbsp;
        </th>
    </tr>
    </thead>
    <?php

    ?>
    <tbody>
    @foreach ($courseGroups as $groups)
        <tr>
            <td><a class="icon-edit" href='{{route('course_group.edit', ['id'=>$groups->id])}}'>&nbsp;</a></td>
            <td>{{$groups->title}}</td>
            <td>{{$groups->step}}</td>
            <td>{{$groups->active}}</td>
            <td>{{$groups->lang}}</td>
            <td><a class="icon-list-ul" href='{{route('course.index')}}?group_id={{$groups->id}}'>&nbsp;</a></td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ $courseGroups->appends(request()->input())->links('pagination.default') }}
@endsection
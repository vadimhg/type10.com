@extends('layouts.admin')
@section('content')
    <h1>Edit "{{ $courseGroup->title }}" Group</h1>

    <!-- if there are creation errors, they will show here -->
    {{ Html::ul($errors->all(), ['class'=>'alert alert-danger']) }}

    {{ Form::model($courseGroup, array('route' => array('course_group.update', $courseGroup->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('title', 'Title') }}
        {{ Form::text('title', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('step', 'Step') }}
        {{ Form::number('step', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('active', 'Active') }}
        {{ Form::text('active', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('lang', 'Lang') }}
        {{ Form::text('lang', null, array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Save Course Groups', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}
@endsection
@section('scripts')
    @parent
    <script type="text/javascript">
        //todo: javascript validation here
    </script>
@endsection
@extends('layouts.admin')
@section('content')
<div class="panel panel-default">
    <h2>Edit</h2>
    <div class="list-group">
        <a href="{{route('course_group.index')}}" class="list-group-item list-group-item-action">Course Groups</a>
        <a href="{{route('quote.index')}}" class="list-group-item list-group-item-action">Quotes</a>
        <a href="{{route('text.index')}}" class="list-group-item list-group-item-action">Texts</a>
    </div>

    <hr/>
    <h2>Export</h2>
    <div class="list-group">
        <a href="{{route('export_groups')}}" class="list-group-item list-group-item-action">Export Groups</a>
        <a href="{{route('export_courses')}}" class="list-group-item list-group-item-action">Export Courses</a>
        <a href="{{route('export_lessons')}}" class="list-group-item list-group-item-action">Export Lessons</a>
        <a href="{{route('export_pages')}}" class="list-group-item list-group-item-action">Export Pages</a>
        <a href="{{route('export_quotes')}}" class="list-group-item list-group-item-action">Export Quotes</a>
        <a href="{{route('export_texts')}}" class="list-group-item list-group-item-action">Export Texts</a>
        <a href="{{route('export_exams')}}" class="list-group-item list-group-item-action">Export Exams</a>
    </div>
</div>
@endsection
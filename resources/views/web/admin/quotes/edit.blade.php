@extends('layouts.admin')
@section('content')

    <h1>Edit "{{ $quote->id }}" Quote</h1>
    <!-- if there are creation errors, they will show here -->
    {{ Html::ul($errors->all(), ['class'=>'alert alert-danger']) }}

    {{ Form::model($quote, array('route' => array('quote.update', $quote->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('img', 'img') }}
        {{ Form::text('img', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('text', 'text') }}
        {{ Form::textarea('text', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('color', 'color') }}
        {{ Form::text('color', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('size', 'size') }}
        {{ Form::text('size', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('type', 'type') }}
        {{ Form::text('type', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('lang', 'lang') }}
        {{ Form::text('lang', null, array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Save Quote', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}
@endsection
@section('scripts')
    @parent
    <script type="text/javascript">
        //todo: javascript validation here
        function quoteRebuild(){
            $('.quotes-bar').css({
                'background':'url(\'/content/quotes/'+$('input[name="img"]').val()+'\') no-repeat #FFFFE0'
            }).find('div').css({
                'color':$('input[name="color"]').val(),
                'font-size':$('input[name="size"]').val()+'px'
            }).html($('textarea[name="text"]').val());
        }
        $(document).on('change', 'input, textarea', quoteRebuild);
        $(document).on('keyup', 'input, textarea', quoteRebuild);
    </script>
@endsection
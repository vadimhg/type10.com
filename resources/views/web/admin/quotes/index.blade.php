@extends('layouts.admin')
@section('content')
    {{ Form::open(array('route' => 'quote.index', 'method' => 'GET'))}}
    <div class="form-group">
        {{ Form::text('img', request('img'), array('class' => 'form-control')) }}
    </div>
    {{ Form::submit('Filter Quote', array('class' => 'btn btn-primary')) }}
    {{ Form::close() }}
    <table class="table table-striped table-hover table-bordered" id="editabledatatable">
        <thead>
        <tr role="row">
            <th>
                Edit
            </th>
            <th>
                &nbsp;
            </th>
            <th>
                S
            </th>
            <th>
                T
            </th>
            <th>
                A
            </th>
            <th>
               L
            </th>
        </tr>
        </thead>
        <?php

        ?>
        <tbody>
        @foreach ($quotes as $quote)
            <tr>
                <td><a class="icon-edit" href='{{route('quote.edit', ['id'=>$quote->id])}}'>&nbsp;</a></td>
                <td>@include('includes.quote')</td>
                <td>{{$quote->sound}}</td>
                <td>{{$quote->type}}</td>
                <td>{{$quote->author}}</td>
                <td>{{$quote->lang}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $quotes->appends(request()->input())->links('pagination.default') }}
@endsection
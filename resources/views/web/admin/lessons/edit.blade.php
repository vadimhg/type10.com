@extends('layouts.admin')
@section('content')
    <h1>Edit "{{ $lesson->title }}"</h1>

    <!-- if there are creation errors, they will show here -->
    {{ Html::ul($errors->all(), ['class'=>'alert alert-danger']) }}

    {{ Form::model($lesson, array('route' => array('lesson.update', $lesson->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('title', 'Title') }}
        {{ Form::text('title', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('description', 'description') }}
        {{ Form::text('description', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('content', 'content') }}
        {{ Form::textarea('content', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('step', 'step') }}
        {{ Form::text('step', null, array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Save Lesson', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}
@endsection
@section('scripts')
    @parent
    <script type="text/javascript">
        //todo: javascript validation here
    </script>
@endsection
@extends('layouts.admin')
@section('content')
    <table class="table table-striped table-hover table-bordered" id="editabledatatable">
        <thead>
        <tr role="row">
            <th>
                Edit
            </th>
            <th>
                Title
            </th>
            <th>
                Description
            </th>
            <th>
                Step
            </th>
            <th>
                Content
            </th>
            <th>
                Course
            </th>
            <th>
                &nbsp;
            </th>
        </tr>
        </thead>
        <?php

        ?>
        <tbody>
        @foreach ($lessons as $lesson)
            <tr>
                <td><a class="icon-edit" href='{{route('lesson.edit', ['id'=>$lesson->id])}}'>&nbsp;</a></td>
                <td>{{$lesson->title}}</td>
                <td>{{$lesson->desccription}}</td>
                <td>{{$lesson->step}}</td>
                <td>{{$lesson->content}}</td>
                <td>{{$lesson->course->title}}</td>
                <td><a class="icon-list-ul" href='{{route('page.index')}}?lesson_id={{$lesson->id}}'>&nbsp;</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $lessons->appends(request()->input())->links('pagination.default') }}
@endsection
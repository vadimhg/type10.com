@extends('layouts.admin')
@section('content')
    <table class="table table-striped table-hover table-bordered" id="editabledatatable">
        <thead>
        <tr role="row">
            <th>
                Edit
            </th>
            <th>
                Title
            </th>
            <th>
                Step
            </th>
            <th>
                Content
            </th>
            <th>
                Lesson
            </th>
            <th>
                &nbsp;
            </th>
        </tr>
        </thead>
        <?php

        ?>
        <tbody>
        @foreach ($pages as $page)
            <tr>
                <td><a class="icon-edit" href='{{route('page.edit', ['id'=>$page->id])}}'>&nbsp;</a></td>
                <td>{{$page->title}}</td>
                <td>{{$page->step}}</td>
                <td>{{$page->content}}</td>
                <td>{{$page->lesson->title}}</td>
                <td><a class="icon-list-ul" href='{{route('exam.index')}}?page_id={{$page->id}}'>&nbsp;</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $pages->appends(request()->input())->links('pagination.default') }}
@endsection
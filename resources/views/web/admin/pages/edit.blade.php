@extends('layouts.admin')
@section('content')
    <h1>Edit "{{ $page->title }}" Page</h1>

    <!-- if there are creation errors, they will show here -->
    {{ Html::ul($errors->all(), ['class'=>'alert alert-danger']) }}

    {{ Form::model($page, array('route' => array('course.update', $page->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('title', 'Title') }}
        {{ Form::text('title', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('content', 'content') }}
        {{ Form::textarea('content', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('step', 'step') }}
        {{ Form::text('step', null, array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Save Page', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}
@endsection
@section('scripts')
    @parent
    <script type="text/javascript">
        //todo: javascript validation here
    </script>
@endsection
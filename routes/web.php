<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('welcome');})->name('welcome');
Auth::routes();
Route::group(['prefix' => 'student', 'middleware'=>['auth', 'quote', 'path_data', 'breadcrumbs'], 'namespace'=>'web\student'], function()
{
    Route::get('/', 'IndexController@index')->name('student_index');
    Route::group(['prefix' => 'course'],function(){
        Route::get('{course}/lessons', 'CourseController@lessons')->where('course', '[0-9]+')->name('student_lessons');
        Route::get('{course}/{lesson}/pages', 'CourseController@pages')->where(['course'=>'[0-9]+', 'lesson'=>'[0-9]+'])->name('student_pages');
        Route::get('{course}/{lesson}/{page}/page', 'CourseController@page')->where(['course'=>'[0-9]+', 'lesson'=>'[0-9]+', 'page'=>'[0-9]+'])->name('student_page');
    });
});
Route::group(['prefix' => 'admin', 'middleware'=>'auth', 'namespace'=>'web\admin'], function()
{
    Route::group(['middleware'=>['role:admin', 'quote']], function()
    {
        Route::get('/', 'IndexController@index')->name('admin_index');
        Route::resource('course_group', 'CourseGroupController');
        Route::resource('course', 'CourseController');
        Route::resource('lesson', 'LessonController');
        Route::resource('page', 'PageController');
        Route::resource('exam', 'ExamController');
        Route::resource('text', 'TextController');
        Route::resource('quote', 'QuoteController');
        Route::group(['prefix' => 'export'], function()
        {
            Route::get('groups', 'ExportController@groups')->name('export_groups');
            Route::get('courses', 'ExportController@courses')->name('export_courses');
            Route::get('lessons', 'ExportController@lessons')->name('export_lessons');
            Route::get('pages', 'ExportController@pages')->name('export_pages');
            Route::get('quotes', 'ExportController@quotes')->name('export_quotes');
            Route::get('texts', 'ExportController@texts')->name('export_texts');
            Route::get('exams', 'ExportController@exams')->name('export_exams');
        });
    });
});